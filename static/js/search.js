var source = null;
var template = null;

$.get("/templates/result.handlebars", data => { // テンプレートの読み込み
    source = data;
    template = Handlebars.compile(source);
});

$("#search").on("click", function() { // 検索ボタンを押すと、検索結果を読み込み、テンプレートを使用して表示させる
    var keyword = $("#keyword").val();
    if (keyword) {
        $.get("/search/" + keyword, function(data) {
            var html = template({results : data});
            $("#results").html(html);
        });
    }
});

$(".input-field").each(function (index, element) { // エンターキーを押すと、検索ボタンのクリックイベントを生成する
    if ($(element).attr("submit")) {
        $(element).on("keyup", function (event) { 
            if (event.which == 13) { // エンターキー
                $("#" + $(element).attr("submit")).click();
            }
        });
    }
});

